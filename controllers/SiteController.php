<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Entradas;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;


class SiteController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionListar()
    {
        $s=Entradas::find()->asArray()->all();
        return $this->render("listarTodos", ["datos" => $s]);
    }
    
    public function actionListar1() 
    {
        $s=Entradas::find()->all();
        return $this->render("listarTodos",["datos"=>$s]);
    }
    
    public function actionListar2()
    {
        $salida=Entradas::find()->select(['texto'])->asArray()->all();
        return $this->render("listarTodos",['datos'=> $salida]);
    }
    
    public function actionListar3()
    {
        $salida=Entradas::find()->select(['texto'])->all();
        return $this->render("listarTodos",['datos'=> $salida]);
    }
    
    public function actionListar4()
    {
        $salida= new Entradas();
        return $this->render('listarTodos',['datos' => $salida->find()->all()]);
    }
    
    public function actionListar5()
    {
        $salida=new Entradas();
        return $this->render('listarTodos',['datos' => $salida->findOne(1)]);
    }
    
    public function actionListar6()
    {
        return $this->render('listarTodos',['datos' => Yii::$app->db->createCommand("Select * from entradas")->queryAll()]);
    }
    
    public function actionMostrar()
    {
        $dataProvider=new ActiveDataProvider(['query'=> Entradas::find()]);
        return $this->render('mostrar',['dataProvider' => $dataProvider]);
    }
    
    public function actionMostraruno()
    {
        return $this->render("mostraruno",["model" => Entradas::findOne(1)]);
    }
    
    public function actionConsulta1()
    {
        //mostrar todos los textos de la tabla entradas
        //voy a realizar la consulta utilizando directamente createComand y SQL 
        $salida=Yii::$app->db->createCommand("select texto from entradas");
        $salida1=$salida->queryAll();
        return $this->render("vistaConsulta1", ["registros" => $salida1]);
    }
    
    public function actionConsulta2()
    {
        //mostrar todos los id de la tabla entradas
        //voy a realizar la consulta utilizando directamente createComand y SQL 
        $salida=Yii::$app->db->createCommand("select id from entradas");
        $salida1=$salida->queryAll();
        return $this->render("vistaConsulta2", ["registros" => $salida1]);
    }
    
    public function actionConsulta3()
    {
        //mostrar todos los textos de la tabla entradas
        //voy a realizar la consulta utilizando activeRecord
        $salida=Entradas::find()->select('texto'); //esto es un activeQuery
        $salida1=$salida->all(); //array de activeRecords
        return $this->render("vistaConsulta3",["registros" => $salida1]);
    }
    
    public function actionConsulta4()
    {
        //mostrar todos los id de la tabla entradas
        //voy a realizar la consulta utilizando activeRecord
        $salida=Entradas::find()->select('id')->all(); //esto es un activeQuery
        return $this->render("vistaConsulta4", ["registros" => $salida]);
    }
    
    public function actionConsulta5()
    {
        //mostrar todos los campos de la tabla entradas
        //voy a realizar la consulta utilizando directamente createComand y SQL
        $salida=Yii::$app->db->createCommand("select * from entradas")->queryAll();
        return $this->render("vistaConsulta5", ["registros" => $salida]);
    }
    
    public function actionConsulta6()
    {
        //mostrar todos los campos de la tabla entradas
        //voy a realizar la consulta utilizando activeRecord
        $salida=Entradas::find()->all(); //esto es un activeQuery
        return $this->render("vistaConsulta6", ["registros" => $salida,"modelo" => new Entradas]);
    }
    
    public function actionConsulta7()
    {
        //mostrar todos los registros de la tabla entradas para mostrarlos en un Gridview
        
        $dataProvider = new ActiveDataProvider([
        'query' => Entradas::find()]);
        return $this->render("vistaConsulta7",["dataProvider" => $dataProvider]);
    }
    
    public function actionConsulta8()
    {
        //mostrar todos los textos de la tabla entradas para mostrarlos en un Gridview
        
        $dataProvider = new ActiveDataProvider([
        'query' => Entradas::find()->select('texto')]);
        return $this->render("vistaConsulta8",["dataProvider" => $dataProvider]);
    }
    
    public function actionConsulta9()
    {
        //mostrar todos los registros de la tabla entradas para mostrarlos en un Listview
        
        $dataProvider = new ActiveDataProvider([
        'query' => Entradas::find()]);
        return $this->render("vistaConsulta9",["dataProvider" => $dataProvider]);
    }
    
    public function actionConsulta10()
    {
        //mostrar todos los textos de la tabla entradas para mostrarlos en un Listview
        
        $dataProvider = new ActiveDataProvider([
        'query' => Entradas::find()->select('texto')]);
        return $this->render("vistaConsulta10",["dataProvider" => $dataProvider]);
    }
    
    public function actionConsulta11()
    {
        //mostrar el texto de la noticia con id=1 ulilizando un detailview
        
        $registroActivo= Entradas::find()->select('texto')->where('id=1')->one();
        return $this->render('vistaConsulta11',["modelo" => $registroActivo]);
    }
    
    public function actionConsulta12()
    {
        //mostrar todos los registros de entradas ulilizando sqldataprovider y gridview
        $datos=new SqlDataProvider([
            'sql' => 'select * from entradas',
        ]);
        return $this->render("vistaConsulta7",["dataProvider" => $datos]);
    }
    
    
}
